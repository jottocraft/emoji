const { Worker } = require('worker_threads');
const path = require("path");

//Open 3 SVGO workers
const svgoWorkers = [];
for (let i = 0; i < 3; i++) svgoWorkers.push({
    worker: new Worker(path.join(__dirname, "svgo.js")),
    runningTask: null,
    i
});

//Keep track of queued tasks
const taskQueue = [];

//Listen for worker availability
svgoWorkers.map(worker => {
    worker.worker.on("message", data => {
        if (data === "complete") {
            //Completed task
            if (worker.runningTask?.res) worker.runningTask.res();
            worker.runningTask = null;
            runNextTask(worker);
        }
    });
});

function runNextTask(worker) {
    let availWorker = worker;
    if (!availWorker) {
        //Find an available worker if we haven't been referred to by one
        availWorker = svgoWorkers.find(w => !w.runningTask);
        if (!availWorker) return; //Couldn't find a free worker; need to wait for one to free up
    }

    const nextTask = taskQueue.shift(); //get the next action
    if (!nextTask) return;

    //Set the thread to run this action
    availWorker.runningTask = nextTask;
    
    //Run it!
    availWorker.worker.postMessage(nextTask.msg);
}

function runSVGOTask(msg) {
    return new Promise((res, rej) => {
        taskQueue.push({ msg, res, rej });
        runNextTask();
    });
}

function closeSVGO() {
    svgoWorkers.forEach(w => w.worker.postMessage("exit"));
}

module.exports = {
    runSVGOTask,
    closeSVGO
};