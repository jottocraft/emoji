const { spawn } = require("child_process");

//Open 3 Inkscape threads in shell mode
const inkscapeThreads = [];
for (let i = 0; i < 4; i++) inkscapeThreads.push({
    process: spawn("inkscape", ["--shell"]),
    runningAction: true,
    i
});

//Keep track of queued actions
const actionQueue = [];

//Initialize listeners & additional options
inkscapeThreads.map(thread => {
    thread.process.stdout.on("data", data => {
        if (String(data).trim().endsWith(">")) {
            if (thread.runningAction?.res) thread.runningAction.res();
            thread.runningAction = null;
            runNextAction(thread);
        }
    });
    thread.process.stderr.on("data", data => console.error(String(data)));
    thread.process.stdin.setDefaultEncoding("utf8");
});

function runNextAction(thread) {
    let availThread = thread;
    if (!availThread) {
        //Find an available thread if we haven't been referred to by one
        availThread = inkscapeThreads.find(t => !t.runningAction);
        if (!availThread) return; //Couldn't find a thread; need to wait for one to free up
    }

    const nextAction = actionQueue.shift(); //get the next action
    if (!nextAction) return;

    //Set the thread to run this action
    availThread.runningAction = nextAction;
    
    //Run it!
    availThread.process.stdin.cork();
    availThread.process.stdin.write(nextAction.cmd + "\n", "utf8");
    availThread.process.stdin.uncork();
}

function runInkscapeAction(cmd) {
    return new Promise((res, rej) => {
        actionQueue.push({ cmd, res, rej });
        runNextAction();
    });
}

function closeInkscape() {
    inkscapeThreads.forEach(thread => thread.process.kill("SIGTERM"));
}

module.exports = {
    runInkscapeAction,
    closeInkscape
};