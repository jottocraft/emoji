const fs = require("fs");
const fsp = require("fs/promises");
const path = require("path");
const { execSync } = require("child_process");
const ProgressBar = require("progress");

//SVGO threads
const { runSVGOTask, closeSVGO } = require("./svgoThreader");

//Inkscape threads
const { runInkscapeAction, closeInkscape } = require("./inkscape");

//Clean up old assets
if (fs.existsSync(path.join(__dirname, "assets"))) {
    console.log("Cleaning up previously compiled assets...");
    fs.rmSync(path.join(__dirname, "assets"), { recursive: true, force: true });
}

//Clean up old organized sources
if (fs.existsSync(path.join(__dirname, "sources", "organized"))) {
    console.log("Cleaning up previously organized sources...");
    fs.rmSync(path.join(__dirname, "sources", "organized"), { recursive: true, force: true });
}

console.log("Downloading and compiling assets...");

function prepareFolder(...location) {
    const DIR = path.join(__dirname, ...location);
    if (!fs.existsSync(DIR)) fs.mkdirSync(DIR);
    return DIR;
}

const SOURCES_DIR = prepareFolder("sources");
const ASSETS_DIR = prepareFolder("assets");
prepareFolder("assets", "highres");
prepareFolder("assets", "flat");

const ORGANIZED_DIR = prepareFolder("sources", "organized");
prepareFolder("sources", "organized", "highres");
prepareFolder("sources", "organized", "flat");


/**
 * Ensures an up-to-date copy of a repo is downloaded
 */
function downloadOrUpdateRepo(name, url, dir) {
    const SOURCE_DIR = path.join(SOURCES_DIR, dir);

    if (fs.existsSync(SOURCE_DIR)) {
        console.log(`\n\nUpdating ${name}...\n`);
        execSync("git pull", {
            cwd: SOURCE_DIR,
            stdio: 'inherit'
        });
    } else {
        console.log(`\n\nDownloading ${name} (this may take a while)...\n`);
        execSync(`git clone ${url} ${dir}`, {
            cwd: SOURCES_DIR,
            stdio: 'inherit'
        });
    }

    console.log("\n\n");
}

/**
 * Organizes source emoji to make processing easier
 */
async function organizeSourceEmoji() {
    console.log("Organizing Fluent UI emoji...");
    const FLUENTUI_EMOJI_ASSETS = path.join(SOURCES_DIR, "fluentui-emoji", "assets");
    const fluentUIFiles = fs.readdirSync(FLUENTUI_EMOJI_ASSETS, { withFileTypes: true });
    const fluentUIProgress = new ProgressBar("[:bar] :current/:total", { total: fluentUIFiles.length });
    await Promise.all(
        fluentUIFiles.map(async (dir) => {
            //There should only be folders
            if (!dir.isDirectory()) return;
            const emojiDir = path.join(FLUENTUI_EMOJI_ASSETS, dir.name);

            const metadata = JSON.parse(await fsp.readFile(path.join(emojiDir, "metadata.json")));

            if (metadata.unicodeSkintones) {
                //Copy emoji variants for each skin tone
                await Promise.all(
                    metadata.unicodeSkintones.map(async (skinToneCodepoint, i) => {
                        const skintoneDir = path.join(emojiDir, FLUENT_SKIN_TONE_DIRS[i]);
                        await organizeFluentEmojiStyles(skintoneDir, skinToneCodepoint.replaceAll(" ", "-"));
                    })
                );
            } else {
                //Copy emoji

                //Check if default folder exists just in case
                //see https://github.com/microsoft/fluentui-emoji/issues/15#issuecomment-1211115438
                const defaultSubdir = path.join(emojiDir, "Default");
                if (fs.existsSync(defaultSubdir)) {
                    //Use default folder since it exists without skin tone variants for some reason
                    await organizeFluentEmojiStyles(defaultSubdir, metadata.unicode.replaceAll(" ", "-"));
                } else {
                    //Use standard folder
                    await organizeFluentEmojiStyles(emojiDir, metadata.unicode.replaceAll(" ", "-"));
                }
            }

            fluentUIProgress.tick();
        })
    );

    console.log("\nOrganizing Noto emoji...");

    const NOTO_EMOJI_ASSETS = path.join(SOURCES_DIR, "noto-emoji", "svg");
    const notoEmojiFiles = fs.readdirSync(NOTO_EMOJI_ASSETS);
    const notoEmojiProgress = new ProgressBar("[:bar] :current/:total", { total: notoEmojiFiles.length });
    await Promise.all(
        notoEmojiFiles.map(async (file) => {
            await organizeNotoEmoji(NOTO_EMOJI_ASSETS, file);
            notoEmojiProgress.tick();
        })
    )

    console.log("\nOrganizing waved country flags...");

    const NOTO_EMOJI_FLAGS = path.join(SOURCES_DIR, "noto-emoji", "third_party", "region-flags", "waved-svg");
    const notoEmojiFlagFiles = fs.readdirSync(NOTO_EMOJI_FLAGS);
    const notoEmojiFlagProgress = new ProgressBar("[:bar] :current/:total", { total: notoEmojiFlagFiles.length });
    await Promise.all(
        notoEmojiFlagFiles.map(async (file) => {
            await organizeNotoEmoji(NOTO_EMOJI_FLAGS, file);
            notoEmojiFlagProgress.tick();
        })
    )

    console.log("\nDone organizing emoji!\n\n");
}

/**
 * Get the file name for organized & processed emoji
 */
function getEmojiPath(codepoint, style, processed) {
    if (processed) return path.join(ASSETS_DIR, style, `${codepoint}.svg`);
    return path.join(ORGANIZED_DIR, style, `${codepoint}.svg`);
}

/**
 * Store an emoji in the organized directory
 */
async function organizeEmoji(source, dest, overwrite = false) {
    if (!overwrite && fs.existsSync(dest)) return;
    if (!String(await fsp.readFile(source)).includes("<svg")) return;
    return fsp.copyFile(source, dest);
}

/**
 * Fluent UI emoji skin tone directory names
 * in the same order as metadata.json unicodeSkintones 
 */
 const FLUENT_SKIN_TONE_DIRS = [
    "Default", "Light", "Medium-Light", "Medium", "Medium-Dark", "Dark"
];

/**
 * Organizes fluent emoji styles
 */
async function organizeFluentEmojiStyles(emojiDir, codepoint) {
    async function organizeStyle(styleDir, style) {
        const stylePath = path.join(emojiDir, styleDir);
        if (!fs.existsSync(stylePath)) {
            console.error("Attempted to access non-existent Fluent emoji style", stylePath);
            return;
        }

        const files = await fsp.readdir(stylePath);
        const svgFile = files.find(f => path.extname(f).toUpperCase() === ".SVG");
        if (!svgFile) {
            console.error("Couldn't find an SVG file for Fluent emoji style", stylePath);
            return;
        }

        await organizeEmoji(path.join(stylePath, svgFile), getEmojiPath(codepoint, style, false), true);
    }

    await Promise.all([
        //Store highres emoji style
        organizeStyle("Color", "highres"),

        //Store flat emoji style
        organizeStyle("Flat", "flat")
    ])
}

async function organizeNotoEmoji(dir, file) {
    const codepoint = file.match(/^emoji_u(.*?)\.svg$/)?.[1]?.replaceAll("_", "-");
    if (!codepoint) return;

    await Promise.all([
        organizeEmoji(path.join(dir, file), getEmojiPath(codepoint, "flat", false), false),
        organizeEmoji(path.join(dir, file), getEmojiPath(codepoint, "highres", false), false)
    ]);
}

/**
 * Processes and stores an emoji asset
 * @param {string} fileName The name of the source file
 * @param {"highres" | "flat"} style The style variant of the emoji
 */
async function processEmoji(fileName, style) {
    const filePath = path.join(ORGANIZED_DIR, style, fileName);
    const codepoint = fileName.slice(0, -4);

    const destPath = getEmojiPath(codepoint, style, true);

    //Crop files using inkscape since it just works without being annoying
    await runInkscapeAction(`file-open:${filePath}; page-fit-to-selection; export-plain-svg; export-filename:${destPath}; export-do; file-close`);
    
    //Optimize files with SVGO for final distribution
    await runSVGOTask({
        i: destPath,
        o: destPath,
        codepoint
    });
}

async function processAllEmoji() {
    console.log("Processing emoji...");

    const flatEmojiFiles = fs.readdirSync(path.join(ORGANIZED_DIR, "flat")), highresEmojiFiles = fs.readdirSync(path.join(ORGANIZED_DIR, "highres"));
    const processingProgress = new ProgressBar("[:bar] :current/:total", { total: flatEmojiFiles.length + highresEmojiFiles.length });

    const flatEmojiPromises = Promise.all(
        flatEmojiFiles.map(async (file) => {
            await processEmoji(file, "flat");
            processingProgress.tick();
        })
    )

    const highresEmojiPromises = Promise.all(
        highresEmojiFiles.map(async (file) => {
            await processEmoji(file, "highres");
            processingProgress.tick();
        })
    )

    await flatEmojiPromises;
    await highresEmojiPromises;
}

(async function () {
    const startTime = new Date();

    downloadOrUpdateRepo("Fluent UI emoji sources", "https://github.com/microsoft/fluentui-emoji.git", "fluentui-emoji");
    downloadOrUpdateRepo("Noto emoji sources", "https://github.com/googlefonts/noto-emoji.git", "noto-emoji");

    //This might be useful but isn't currently utilized
    //downloadOrUpdateRepo("Emoji metadata", "https://github.com/googlefonts/emoji-metadata.git", "emoji-metadata");

    //console.log("\n\nCompiling emoji assets...");

    //const METADATA_FILE = path.join(SOURCES_DIR, "emoji-metadata", "emoji_14_0_ordering.json");
    //console.log("Using metadata from", METADATA_FILE);

    await organizeSourceEmoji();
    await processAllEmoji();

    const duration = new Date().getTime() - startTime.getTime();
    const min = Math.round(duration / 60000);
    const sec = Math.round((duration % 60000) / 1000);
    console.log(`Done after ${min}min ${sec}sec! 🎉🪅`);
})().then(() => {
    closeInkscape();
    closeSVGO();
});