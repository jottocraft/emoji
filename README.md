### jottocraft emoji service

The jottocraft emoji service serves emoji sourced from multiple open-source projects to provide both the highest-quality emoji when available, and fallbacks for compatibility with the full range of characters.

Emoji are sourced and prioritized in this order:

1. [Microsoft Fluent UI Emoji](https://github.com/microsoft/fluentui-emoji)
    - `highres` and `flat` emoji variants
    - Covers most of Emoji 14.0 (No paperclip, technologist, country flags, video games)
2. [Google Noto Emoji](https://github.com/googlefonts/noto-emoji)
    - `flat` emoji variants only
    - Fully covers Emoji 15.0

For speed and cache purposes, the service serves static SVG files only.

## Compiling emojis

**System requirements**:

- Inkscape installed and accessible via the CLI (in PATH)
- A machine (ideally Windows) with *at least* 8 CPU cores
- At least 4 (ideally 8+) gigabytes of system memory
- Node JS
- git

**Build steps:**

- `npm install`
- `npm run build`

Compiled emoji will be stored in the `assets` directory.

### Usage

The jottocraft emoji service is primarily intended to be consumed by [Shamrock XP's Icon component](https://bitbucket.org/jottocraft/shamrock/src/main/package/src/react/Icon.tsx).

The live URL format is as follows:

`https://emoji.jottocraft.com/(highres | flat)/(codepoint).svg`

If a `highres` variant isn't available, it will fallback to a `flat` version.

The `codepoint` should be the Unicode character codepoint, separated by dashes, lowercase only. E.g. `1f1fA-1f1f8` for the American flag.

-----------

NOTE: The jottocraft emoji service is not supported nor endorsed by Microsoft or Google, and makes no availability guarantees or warranties. All emoji are owned and copyrighted by their respective owners.