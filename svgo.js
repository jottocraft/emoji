const { parentPort } = require('worker_threads');
const { optimize } = require('svgo');
const fsp = require("fs/promises");

parentPort.on("message", async (message) => {
    if (message === "exit") {
        parentPort.close();
    } else {
        const svgData = await fsp.readFile(message.i);

        const opt = optimize(svgData, {
            path: message.i,
            multipass: true,
            plugins: [
                {
                    name: 'preset-default',
                    params: {
                        overrides: {
                            //Keep inkscape's beautifully-crafted viewbox
                            removeViewBox: false,

                            //This breaks Fluent UI emoji
                            moveElemsAttrsToGroup: false
                        }
                    }
                },
                'removeDimensions' //Remove hardcoded width and height attributes for better scaling
            ]
        });

        if (!opt.data) {
            console.error("[E] SVGo returned no output for emoji " + message.codepoint);
            return;
        }

        await fsp.writeFile(message.o, opt.data);

        parentPort.postMessage("complete");
    }
});